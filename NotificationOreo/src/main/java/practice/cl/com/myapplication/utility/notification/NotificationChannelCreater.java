package practice.cl.com.myapplication.utility.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;
import practice.cl.com.myapplication.utility.other.Log;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class NotificationChannelCreater {

    private final Context context;
    private String[] channelNames, channelID, channelPatterns, channelDescription;
    private TypedArray channelColors, channelLedStatus, channelVibrationStatus;
    private NotificationManager notificationManager;


    public NotificationChannelCreater(final Context context) {
        if (context instanceof ParentActivity && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.context = context;
            createChannels();
        } else {
            this.context = null;
        }
    }

    private void createChannels() {
        if (context == null) {
            return;
        }

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        initArrays();

        for (String id : channelID) {
            createNotificationChannels(Integer.parseInt(id));
        }
    }


    private void initArrays() {
        channelNames = context.getResources().getStringArray(R.array.channel_names);
        channelPatterns = context.getResources().getStringArray(R.array.channel_vibration_pattern);
        channelDescription = context.getResources().getStringArray(R.array.channel_description);
        channelColors = context.getResources().obtainTypedArray(R.array.channel_colors);
        channelID = context.getResources().getStringArray(R.array.channel_id);
        channelLedStatus = context.getResources().obtainTypedArray(R.array.channel_led_status);
        channelVibrationStatus = context.getResources().obtainTypedArray(R.array.channel_vibration_status);
    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannels(final int id) {
        NotificationChannel notificationChannel;
        Log.e("id vale", "id : " + id + " channelID " + channelID[id] + "  channelNames " + channelNames[id]);
        switch (id) {
            case 0:
                // Silent Channel
                notificationChannel = new NotificationChannel(channelID[id],
                        channelNames[id], NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
                break;
            case 1:
                // Updates Channel
                notificationChannel = new NotificationChannel(channelID[id],
                        channelNames[id], NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                break;
            case 2:
                // Important Channel
                notificationChannel = new NotificationChannel(channelID[id],
                        channelNames[id], NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                break;
            case 3:
                // Urgent Channel
                notificationChannel = new NotificationChannel(channelID[id],
                        channelNames[id], NotificationManager.IMPORTANCE_MAX);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                break;
            default:
                // Default Channel
                notificationChannel = new NotificationChannel(channelID[id],
                        channelNames[id], NotificationManager.IMPORTANCE_NONE);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                break;
        }
        createChannel(id, notificationChannel);

    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel(final int id, final NotificationChannel notificationChannel) {
        notificationChannel.setDescription(channelDescription[id]);

        if (channelLedStatus.getBoolean(id, false)) {
            notificationChannel.enableLights(true);
            //notificationChannel.setLightColor(ContextCompat.getColor(context, channelColors[id]));
        } else {
            notificationChannel.enableLights(false);
        }

        if (channelVibrationStatus.getBoolean(id, false)) {
            notificationChannel.enableVibration(true);

            String[] patternString = channelPatterns[id].split(" ");
            long[] pattern = new long[patternString.length];
            for (int i = 0; i < pattern.length; i++) {
                pattern[i] = Long.valueOf(patternString[i]);
            }

            notificationChannel.setVibrationPattern(pattern);
        } else {
            notificationChannel.enableVibration(false);
        }

        notificationManager.createNotificationChannel(notificationChannel);
    }
}
