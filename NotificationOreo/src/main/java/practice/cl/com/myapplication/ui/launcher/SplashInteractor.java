package practice.cl.com.myapplication.ui.launcher;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public interface SplashInteractor {

    /**
     * to validate user access token
     */
    void validateToken(final ResponseListener listener);

    /**
     * to validate user data (to check that number and email are verified or not)
     */
    void validateUserData();

    /**
     * to create notification channels
     */
    void createNotificationChannel();

    /**
     * check if tutorial screen is shown a specific number of times
     * if not then show tutorials else goes to login
     *
     * @return true if times tutorial show is less than count set
     */
    boolean showTutorial();

    /**
     * to check if user data is already present on application side
     *
     * @return true if user data is present
     */
    boolean isUserDataPresent();


    interface ResponseListener {
        void onError(final String error);

        void onSuccess(final Class destination);

        void onSessionExpired(final String error);
    }


}
