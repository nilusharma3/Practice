package practice.cl.com.myapplication.ui.launcher;

import android.content.Context;

import io.paperdb.Paper;
import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.models.userData.UserData;
import practice.cl.com.myapplication.ui.common.activity.ParentInteractorImpl;
import practice.cl.com.myapplication.ui.homeScreen.homeScreenActivity.Home;
import practice.cl.com.myapplication.ui.otpVerification.OTPVerification;
import practice.cl.com.myapplication.ui.tutorials.TutorialsActivity;
import practice.cl.com.myapplication.utility.notification.NotificationChannelCreater;
import practice.cl.com.myapplication.utility.other.Log;

import static practice.cl.com.myapplication.utility.other.AppConstants.ARE_NOTIFICATION_CHANNELS_CREATED;
import static practice.cl.com.myapplication.utility.other.AppConstants.PAPER_BOOK_NAME;
import static practice.cl.com.myapplication.utility.other.AppConstants.TUTORIAL_COUNT;
import static practice.cl.com.myapplication.utility.other.AppConstants.TUTORIAL_SHOWN_COUNT;

/**
 * Created by socomo on 10/27/17.
 */

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public class SplashInteractorImpl extends ParentInteractorImpl implements SplashInteractor {

    private Context context;
    private UserData userData = null;
    private ResponseListener listener;


    public SplashInteractorImpl(final Context context) {
        this.context = context;
        createNotificationChannel();
    }

    @Override
    public void validateToken(final ResponseListener listener) {
        this.listener = listener;
        if (true) {
            //token hit get user data in response
            // update user data
            validateUserData();
        } else {
            // user session expired
            if (/*session expired */ true) {
                listener.onSessionExpired(context.getString(R.string.session_expire));
            } else {
                listener.onError("message to display");
            }
        }
    }

    @Override
    public void validateUserData() {

        if (true /* user is verified*/) {
            //phone is verified
            if (showTutorial()) {
                listener.onSuccess(TutorialsActivity.class);
            } else {
                listener.onSuccess(Home.class);
            }
        } else if (true /* is not verified*/) {
            //if phone not verified
            listener.onSuccess(OTPVerification.class);
        }

    }


    /**
     * checks weather notification channels are created
     * if not created than creates notification channels
     */
    @Override
    public void createNotificationChannel() {
        if (!Paper.book(PAPER_BOOK_NAME).contains(ARE_NOTIFICATION_CHANNELS_CREATED)) {
            new NotificationChannelCreater(context);
            Paper.book(PAPER_BOOK_NAME).write(ARE_NOTIFICATION_CHANNELS_CREATED, true);
        }
    }

    @Override
    public boolean showTutorial() {
        int count = 0;
        boolean showTutorial = false;
        if (Paper.book().contains(TUTORIAL_SHOWN_COUNT)) {
            count = Paper.book().read(TUTORIAL_SHOWN_COUNT);
        }
        if (count <= TUTORIAL_COUNT) {
            showTutorial = true;
        }
        Log.e("tut count", "  :  " + count);
        Paper.book().write(TUTORIAL_SHOWN_COUNT, count++);
        return showTutorial;
    }

    @Override
    public boolean isUserDataPresent() {
        return super.isUserDataPresent();
    }

    @Override
    public UserData getUserData() {
        return super.getUserData();
    }
}
