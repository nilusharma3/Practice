package practice.cl.com.myapplication.ui.homeScreen.homeScreenActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import practice.cl.com.myapplication.R;

import practice.cl.com.myapplication.utility.notification.NotificationCreator;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class HomePresenterImpl implements HomePresenter {

    private HomeInteractor interactor;
    private Context context;
    private NotificationCreator notificationCreator;
    private HomeView homeView;


    public HomePresenterImpl(final Context context) {
        this.context = context;
        homeView = (HomeView) context;
        init();
    }

    @Override
    public void init() {
        interactor = new HomeInteractorImpl(context);
        homeView.initView();
        homeView.initListeners();
    }

    @Override
    public void onButtonClickEvent(final int id) {
        switch (id) {
            /*case R.id.btnSilent:
                homeView.showProgress(context);
                //showNotification(CHANNEL_SILENT, true);
                break;
            case R.id.btnUpdates:
                showNotification(CHANNEL_UPDATES, true);
                break;
            case R.id.btnImportant:
                showNotification(CHANNEL_IMPORTANT, false);
                break;
            case R.id.btnUrgent:
                showNotification(CHANNEL_URGENT, false);
                break;
            case R.id.btnDefault:
                showNotification(CHANNEL_DEFAULT, false);
                break;*/

            default:
                break;
        }
    }

    @Override
    public void initNavList(final ListView listView) {

    }

    @Override
    public void onNavItemClicked(final int position) {
        switch (position) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    private void showNotification(final int channelId, final boolean isAutoCancel) {
        if (notificationCreator == null) {
            notificationCreator = new NotificationCreator(context);
        }
        notificationCreator.createCustomNotification(context.getString(R.string.app_name),
                "message to display", isAutoCancel, channelId, Home.class, new Bundle());
    }
}
