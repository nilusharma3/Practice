package practice.cl.com.myapplication.ui.common.activity;

import io.paperdb.Paper;
import practice.cl.com.myapplication.models.userData.UserData;

import static practice.cl.com.myapplication.utility.other.AppConstants.USER_DATA;

/**
 * Created by socomo on 11/2/17.
 */

public class ParentInteractorImpl implements ParentInteractor {


    private UserData userData = null;

    @Override
    public UserData getUserData() {
        if (userData != null && Paper.book().contains(USER_DATA)) {
            userData = Paper.book().read(USER_DATA, null);
        }
        return userData;
    }

    @Override
    public boolean isUserDataPresent() {
        if (userData != null) {
            return true;
        } else {
            userData = getUserData();
            return userData != null;
        }
    }


}
