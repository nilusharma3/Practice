package practice.cl.com.myapplication.ui.common.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.utility.dialogs.ProgressDialog;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */
public abstract class ParentActivity
        extends AppCompatActivity
        implements ParentView, View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ProgressDialog.hideProgressDialog();
    }


    @Override
    public void showProgress(final Context context) {
        ProgressDialog.showProgressDialog(context, context.getString(R.string.please_wait), true);
    }

    @Override
    public void hideProgress() {
        ProgressDialog.hideProgressDialog();
    }

    @Override
    public void moveTo(final Class destination, final boolean clearStack, final Bundle bundle) {
        Intent intent = new Intent(this, destination);
        if (clearStack) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    @Override
    public void moveTo(final Class destination, final boolean clearStack) {
        moveTo(destination, clearStack, null);
    }


    @Override
    public void moveTo(final Class destination) {
        moveTo(destination, false, null);
    }
}
