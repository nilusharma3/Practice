package practice.cl.com.myapplication.utility.other;

import practice.cl.com.myapplication.BuildConfig;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class Log {
    static final boolean isPrintEnabled = BuildConfig.DEBUG;

    public static void i(final String tag, final String message) {
        if (isPrintEnabled) {
            android.util.Log.i(tag, message);
        }
    }

    public static void e(final String tag, final String message) {
        if (isPrintEnabled) {
            android.util.Log.e(tag, message);
        }
    }

    public static void w(final String tag, final String message) {
        if (isPrintEnabled) {
            android.util.Log.w(tag, message);
        }
    }

    public static void d(final String tag, final String message) {
        if (isPrintEnabled) {
            android.util.Log.d(tag, message);
        }
    }

    public static void printStackTrace(final Exception e) {
        if (isPrintEnabled) {
            e.printStackTrace();
        }
    }
}
