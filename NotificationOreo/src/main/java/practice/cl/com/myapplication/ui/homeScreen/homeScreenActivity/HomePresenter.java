package practice.cl.com.myapplication.ui.homeScreen.homeScreenActivity;

import android.widget.ListView;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public interface HomePresenter {

    /**
     * to initialise
     */
    void init();

    /**
     * @param id
     */
    void onButtonClickEvent(int id);

    void initNavList(final ListView listView);

    void onNavItemClicked(final int position);
}
