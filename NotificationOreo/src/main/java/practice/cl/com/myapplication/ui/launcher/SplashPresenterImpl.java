package practice.cl.com.myapplication.ui.launcher;

import android.content.Context;

import practice.cl.com.myapplication.ui.tutorials.TutorialsActivity;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public class SplashPresenterImpl implements SplashPresenter {

    private Context context;
    private SplashInteractor interactor;
    private SplashView splashView;

    public SplashPresenterImpl(final Splash context) {
        this.context = context;
        interactor = new SplashInteractorImpl(context);
        splashView = (SplashView) context;
    }

    @Override
    public void init() {

        splashView.showProgress(context);

        if (interactor.isUserDataPresent()) {
            interactor.validateToken(this);
        } else {
            splashView.hideProgress();
            //splashView.moveTo(LoginSignup.class, true);
            splashView.moveTo(TutorialsActivity.class, true);
        }


        /////
        splashView.moveTo(TutorialsActivity.class, true);
        /**/

    }

    @Override
    public void onError(final String error) {
        splashView.hideProgress();

    }

    @Override
    public void onSuccess(final Class destination) {
        splashView.hideProgress();
        splashView.moveTo(destination, true);
    }

    @Override
    public void onSessionExpired(final String error) {
        splashView.hideProgress();

    }
}
