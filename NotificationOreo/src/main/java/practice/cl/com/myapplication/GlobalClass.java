package practice.cl.com.myapplication;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import io.paperdb.Paper;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class GlobalClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        Paper.init(this);
    }
}
