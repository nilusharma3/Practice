package practice.cl.com.myapplication.ui.common.activity;

import practice.cl.com.myapplication.models.userData.UserData;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public interface ParentInteractor {

    /**
     * to get user data if stored in Paper DB
     *
     * @return object containing user data
     */
    UserData getUserData();

    /**
     * to check if user data is already present on application side
     *
     * @return true if user data is present
     */
    boolean isUserDataPresent();
}
