package practice.cl.com.myapplication.ui.launcher;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public class Splash extends ParentActivity implements SplashView {

    private SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();

        presenter = new SplashPresenterImpl(Splash.this);
        presenter.init();
    }


    @Override
    public void startActivity(final Context sourceContext,
                              final Class destinationClass, final boolean... clearStack) {

    }

    @Override
    public void initView() {

    }

    @Override
    public void initListeners() {

    }


    @Override
    public void onClick(final View v) {
        return;
    }
}
