package practice.cl.com.myapplication.utility.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import practice.cl.com.myapplication.R;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class ProgressDialog {

    private static Context mContext;
    private static Dialog dialog;

    public static void showProgressDialog(final Context context, final String message,
                                          final boolean showMessage) {

        if (context == null) {
            return;
        }
        mContext = context;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater viewInflater = ((Activity) context).getLayoutInflater();
        final View dialogView = viewInflater.inflate(R.layout.progress_dialog, null);
        if (showMessage) {
            TextView tvMessage = (TextView) dialogView.findViewById(R.id.tvProgressDialog);
            tvMessage.setText(message);
        }
        dialogBuilder.setView(dialogView);
        dialog = dialogBuilder.create();
        dialog.setCancelable(false);

        dialog.show();
    }

    public static void hideProgressDialog() {
        if (dialog != null && !((Activity) mContext).isFinishing() && mContext != null) {
            dialog.dismiss();
            dialog = null;
            mContext = null;
        }
    }


}
