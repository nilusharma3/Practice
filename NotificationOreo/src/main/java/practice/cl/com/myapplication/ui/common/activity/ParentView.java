package practice.cl.com.myapplication.ui.common.activity;

import android.content.Context;
import android.os.Bundle;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public interface ParentView extends FragmentListener {

    /**
     * to show progress dialog
     *
     * @param context context of calling activity
     */
    void showProgress(final Context context);

    /**
     * to show progress dialog
     */
    void hideProgress();

    /**
     * to start new activity using intent
     *
     * @param sourceContext    current activity context
     * @param destinationClass destination class reference
     * @param clearStack       boolean true is stack is to be cleared
     */
    void startActivity(final Context sourceContext,
                       final Class destinationClass, final boolean... clearStack);

    /**
     * initialising view by binding them to respective widgets or view elements
     */
    void initView();

    /**
     * to set event listener on view
     */
    void initListeners();

    /**
     * to start new activity
     *
     * @param destination activity class to which to move
     * @param clearStack  true is stack is to be cleared
     * @param bundle      data in bundle to be sent to destination
     */
    void moveTo(final Class destination, final boolean clearStack, final Bundle bundle);

    /**
     * to start new activity
     * calls moveTo(destination, boolean, bundle)
     * ---- keeping bundle null
     *
     * @param destination activity class to which to move
     * @param clearStack  true is stack is to be cleared
     */
    void moveTo(final Class destination, final boolean clearStack);

    /**
     * to start new activity
     * calls moveTo(destination, boolean ,bundle)
     * ---- keeping boolean always false
     * ---- and keeping bundle null
     *
     * @param destination activity class to which to move
     */
    void moveTo(final Class destination);

}
