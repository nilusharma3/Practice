package practice.cl.com.myapplication.ui.common.fragment;

/**
 * Created by socomo on 11/2/17.
 */

public interface ParentFragmentView extends ActivityListener{

    void init();

    void initListeners();


}
