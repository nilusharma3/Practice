package practice.cl.com.myapplication.ui.tutorials;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;

/**
 * Developer : Anil Sharma
 * Created on 10/31/17.
 */

public class TutorialsActivity extends ParentActivity implements TutorialView,
        ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private TutorialAdapter adapter;
    private TutorialPresenter presenter;
    private ImageView[] dots;
    private LinearLayout pageIndicator;
    private int pageCount;
    private Button btnSkip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new TutorialPresenterImpl(this);
        presenter.setViewToFullScreen();
        setContentView(R.layout.activity_tutorials);
        presenter.init();
    }

    /// Methods of  Tutorial View interface

    @Override
    public void startActivity(final Context sourceContext,
                              final Class destinationClass, final boolean... clearStack) {

    }

    @Override
    public void initView() {
        viewPager = (ViewPager) findViewById(R.id.viewPagerTutorials);
        pageIndicator = (LinearLayout) findViewById(R.id.linearLayoutPageIndicatorDots);
        btnSkip = (Button) findViewById(R.id.btnSkip);
    }

    @Override
    public void initListeners() {
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        btnSkip.setOnClickListener(this);
    }

    @Override
    public void initAdapter(final int[] pageResources) {
        pageCount = pageResources.length;
        adapter = new TutorialAdapter(this, pageResources);
    }

    @Override
    public void setPageIndicators() {

        dots = new ImageView[pageCount];

        for (int i = 0; i < pageCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(this,
                    R.drawable.pager_dot_selector));
            if (i == 0) {
                dots[i].setSelected(true);
            } else {
                dots[i].setSelected(false);
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pageIndicator.addView(dots[i], params);
        }
    }


    /// Methods of ViewPager.OnPageChangeListener interface

    @Override
    public void onPageSelected(final int position) {
        for (int i = 0; i < pageCount; i++) {
            if (i == position) {
                dots[i].setSelected(true);
            } else {
                dots[i].setSelected(false);
            }
        }
    }

    @Override
    public void onPageScrolled(final int position,
                               final float positionOffset, final int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(final int state) {

    }


    @Override
    public void onClick(final View v) {
        presenter.onClickEvent(v.getId());
    }
}
