package practice.cl.com.myapplication.ui.tutorials;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Developer : Anil Sharma
 * Created on 10/31/17.
 */

public class TutorialAdapter extends PagerAdapter {

    private Context context;
    private int[] pages;

    public TutorialAdapter(final Context context, final int[] pages) {
        this.context = context;
        this.pages = pages;
    }

    @Override
    public int getCount() {
        return pages.length;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View pageView = LayoutInflater.from(context).inflate(pages[position], container, false);
        container.addView(pageView);
        return pageView;
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((LinearLayout) object);
    }
}
