package practice.cl.com.myapplication.ui.tutorials;

import practice.cl.com.myapplication.ui.common.activity.ParentView;

/**
 * Developer : Anil Sharma
 * Created on 10/31/17.
 */

public interface TutorialView extends ParentView {

    /**
     * to initialise adapter (Tutorial Adapter)
     *
     * @param pageResources array of layouts
     */
    void initAdapter(final int[] pageResources);

    /**
     * to set page indicator dots
     */
    void setPageIndicators();


}
