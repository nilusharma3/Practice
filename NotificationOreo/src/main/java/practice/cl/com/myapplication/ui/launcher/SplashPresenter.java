package practice.cl.com.myapplication.ui.launcher;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public interface SplashPresenter extends SplashInteractor.ResponseListener {

    void init();

}
