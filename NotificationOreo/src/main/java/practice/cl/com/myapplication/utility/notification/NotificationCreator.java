package practice.cl.com.myapplication.utility.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.utility.other.Log;

import static practice.cl.com.myapplication.utility.other.AppConstants.ONE_MILLI_SEC;
import static practice.cl.com.myapplication.utility.other.AppConstants.THREE_MILLI_SEC;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class NotificationCreator {

    private Context context;
    private String[] channelID;

    public NotificationCreator(final Context context) {
        this.context = context;
        initArray();
    }

    private void initArray() {
        channelID = context.getResources().getStringArray(R.array.channel_id);
    }

    private int getSmallIcon() {
        return android.R.drawable.stat_notify_chat;
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private void notifyUser(final Notification.Builder builder) {
        Log.e("notify ","notification");
        getNotificationManager().notify((int) System.currentTimeMillis(), builder.build());
    }


    public void createCustomNotification(final String title, final String body,
                                         final boolean isAutoCancel, final int channelIndex,
                                         final Class destinaltion, final Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            createCustomNotificationForOreoOrAbove(title, body, isAutoCancel, channelIndex,
                    destinaltion, bundle);
        } else {
            createCustomNotificationForBelowOreo(title, body, isAutoCancel, destinaltion, bundle);
        }
    }

    /**
     * if system api level is below 26
     *
     * @param title        title string
     * @param body         message
     * @param isAutoCancel true if yes
     */
    private void createCustomNotificationForBelowOreo(final String title, final String body,
                                                      final boolean isAutoCancel,
                                                      final Class destinaltion, final Bundle bundle) {
        // The id of the channel.

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(getSmallIcon())
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSound(getDefaultSoundUri())
                        .setLights(ContextCompat.getColor(context, R.color.colorPrimary),
                                THREE_MILLI_SEC, ONE_MILLI_SEC)
                        .setAutoCancel(isAutoCancel);
        setPendingIntent(destinaltion, bundle);
        getNotificationManager().notify((int) System.currentTimeMillis(), mBuilder.build());
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createCustomNotificationForOreoOrAbove(final String title, final String body,
                                                        final boolean isAutoCancel,
                                                        final int channelIndex,
                                                        final Class destinaltion,
                                                        final Bundle bundle) {

        Notification.Builder notificationBuilder = new
                Notification.Builder(context, channelID[channelIndex])
                .setContentTitle(title)
                .setContentText(body)
                .setChannelId(channelID[channelIndex])
                .setSmallIcon(getSmallIcon())
                .setAutoCancel(isAutoCancel);
        setPendingIntent(destinaltion, bundle);
        notifyUser(notificationBuilder);
    }


    /*@TargetApi(Build.VERSION_CODES.O)
    public void createAndNotifyOnDefaultChannel(final String title,
                                                final String body, final boolean isAutoCancel) {
        Notification.Builder notificationBuilder = new
                Notification.Builder(context, channelID[CHANNEL_SILENT])
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(getSmallIcon())
                .setAutoCancel(isAutoCancel)
        notifyUser(notificationBuilder);
    }*/

    private void setPendingIntent(final Class destination, final Bundle bundle) {
        Intent intent = new Intent(context, destination);
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);
    }


    /**
     * Send Intent to load system Notification Settings for this app.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void goToNotificationSettings() {
        Intent i = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
        i.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        context.startActivity(i);
    }

    /**
     * Send intent to load system Notification Settings UI for a particular channel.
     *
     * @param channel Name of channel to configure
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void goToNotificationSettings(String channel) {
        Intent i = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
        i.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        i.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
        context.startActivity(i);
    }


    private Uri getDefaultSoundUri() {
        return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }
}
