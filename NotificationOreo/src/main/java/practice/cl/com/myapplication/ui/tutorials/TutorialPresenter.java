package practice.cl.com.myapplication.ui.tutorials;

/**
 * Developer : Anil Sharma
 * Created on 10/31/17.
 */

public interface TutorialPresenter {

    /**
     * initialise view and listeners and to instantiate interactor
     */
    void init();

    /**
     * make activity full screen
     */
    void setViewToFullScreen();

    /**
     * to handle click events
     *
     * @param viewId view id
     */
    void onClickEvent(final int viewId);
}
