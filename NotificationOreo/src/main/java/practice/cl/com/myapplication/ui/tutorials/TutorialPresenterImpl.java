package practice.cl.com.myapplication.ui.tutorials;

import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;
import practice.cl.com.myapplication.ui.homeScreen.homeScreenActivity.Home;

/**
 * Developer : Anil Sharma
 * Created on 10/31/17.
 */

public class TutorialPresenterImpl implements TutorialPresenter {

    private Context context;
    private TutorialView tutorialView;
    private int[] pages = {R.layout.tutorial_1, R.layout.tutorial_2,
            R.layout.tutorial_3, R.layout.tutorial_4, R.layout.tutorial_5};

    /**
     * constructor
     *
     * @param context context of caller / instantiator
     */
    public TutorialPresenterImpl(final Context context) {
        this.context = context;
        tutorialView = (TutorialView) context;
    }

    @Override
    public void init() {
        tutorialView.initView();
        tutorialView.initAdapter(pages);
        tutorialView.setPageIndicators();
        tutorialView.initListeners();
    }


    /**
     * make activity full screen
     */
    @Override
    public void setViewToFullScreen() {
        ((ParentActivity) context).requestWindowFeature(Window.FEATURE_NO_TITLE);
        ((ParentActivity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onClickEvent(final int viewId) {
        switch (viewId) {
            case R.id.btnSkip:
                tutorialView.moveTo(Home.class, false);
                break;
            default:
                break;
        }
    }

}
