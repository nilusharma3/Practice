package practice.cl.com.myapplication.utility.other;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public final class AppConstants {

    /**
     * Paper DB Keys
     */
    public final static String PAPER_BOOK_NAME = "com.paper.book.name";
    public final static String ARE_NOTIFICATION_CHANNELS_CREATED = "is_notificaton_channel_created";
    public final static String USER_DATA = "user_data";
    public final static String TUTORIAL_SHOWN_COUNT = "tutorial_shown_count";


    /**
     * Notification channel index values
     */
    public final static int CHANNEL_SILENT = 0;
    public final static int CHANNEL_UPDATES = 1;
    public final static int CHANNEL_IMPORTANT = 2;
    public final static int CHANNEL_URGENT = 3;
    public final static int CHANNEL_DEFAULT = 4;


    /**
     * Count Conatants
     */
    public final static int TUTORIAL_COUNT = 5;


    /**
     * Time Units
     */
    public final static int ONE_MILLI_SEC = 1000;
    public final static int TWO_MILLI_SEC = 2000;
    public final static int THREE_MILLI_SEC = 3000;

    /**
     * Status codes
     */
    int SESSION_EXPIRED = 401;
}
