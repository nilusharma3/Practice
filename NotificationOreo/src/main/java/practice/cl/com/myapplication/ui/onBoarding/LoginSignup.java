package practice.cl.com.myapplication.ui.onBoarding;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;

/**
 * Developer : Anil Sharma
 * Created on 10/27/17.
 */

public class LoginSignup extends ParentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
    }


    @Override
    public void showProgress(final Context context) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void startActivity(final Context sourceContext, final Class destinationClass, final boolean... clearStack) {

    }

    @Override
    public void initView() {

    }

    @Override
    public void initListeners() {

    }

    @Override
    public void moveTo(final Class destination, final boolean clearStack) {

    }

    @Override
    public void moveTo(final Class destination) {

    }

    @Override
    public void onClick(final View v) {

    }
}
