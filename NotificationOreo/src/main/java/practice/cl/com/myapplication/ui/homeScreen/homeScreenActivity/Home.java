package practice.cl.com.myapplication.ui.homeScreen.homeScreenActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import practice.cl.com.myapplication.R;
import practice.cl.com.myapplication.ui.common.activity.ParentActivity;

/**
 * Developer : Anil Sharma
 * Created on 10/30/17.
 */

public class Home extends ParentActivity implements HomeView, NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_menu_send));
        setSupportActionBar(toolbar);

        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_icon);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_camera);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.menu_icon));

    }


    @Override
    public void onClick(final View v) {

    }

    @Override
    public void startActivity(final Context sourceContext, final Class destinationClass, final boolean... clearStack) {

    }

    @Override
    public void initView() {

    }

    @Override
    public void initListeners() {

    }

    private void setUpActionBar() {


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return false;
    }
}
